package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
)

const (
	//file names
	candlesFileName    = "candles_5m.csv"
	userTradesFileName = "user_trades.csv"
	resultFileName     = "output.csv"

	//numbers of symbols after point in float64 price value
	defaultFloatBitSize = 2

	//position of the parameters in candle
	tickerPositionInCandle   = 0
	timePositionInCandle     = 1
	maxPricePositionInCandle = 3
	minPricePositionInCandle = 4

	//attempt to use array as an object
	maxValuePosition     = 0
	maxValueTimePosition = 1
	minValuePosition     = 2
	minValueTimePosition = 3

	//position of the parameters in trade
	userIDPositionInTrade        = 0
	tickerPositionInTrade        = 2
	purchasePricePositionInTrade = 3
	sellingPricePositionInTrade  = 4
)

func main() {
	allCandles, err := readAllValuesInFile(candlesFileName)
	if err != nil {
		log.Fatal("error opening file: {", candlesFileName, "}", "check his presence")
	}

	tickersCandles := make(map[string][][]string)
	for _, candle := range allCandles {
		tickersCandles[candle[tickerPositionInCandle]] = append(tickersCandles[candle[tickerPositionInCandle]], candle)
	}

	tickersMaxAndMinValues := calculateMaxAndMinValue(tickersCandles)

	rawTrades, err := readAllValuesInFile(userTradesFileName)
	if err != nil {
		log.Fatal("error opening file: {", userTradesFileName, "}", "check his presence")
	}

	processedTrades := make(map[string]map[string][][]string)
	for _, trade := range rawTrades {
		if processedTrades[trade[userIDPositionInTrade]] == nil {
			processedTrades[trade[userIDPositionInTrade]] = make(map[string][][]string)
		}

		processedTrades[trade[userIDPositionInTrade]][trade[tickerPositionInTrade]] =
			append(processedTrades[trade[userIDPositionInTrade]][trade[tickerPositionInTrade]], trade)
	}

	output := buildOutputValues(processedTrades, tickersMaxAndMinValues)

	err = writeFile(output, resultFileName)
	if err != nil {
		log.Fatal("error creating file {", resultFileName, "}")
	}
}

func readAllValuesInFile(fileName string) ([][]string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)

	result, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	return result, nil
}

func calculateMaxAndMinValue(tickersCandles map[string][][]string) map[string][]string {
	tickersMaxAndMinValues := make(map[string][]string)
	for key, candle := range tickersCandles {
		tickersMaxAndMinValues[key] = append(tickersMaxAndMinValues[key], candle[0][maxPricePositionInCandle], "", candle[0][minPricePositionInCandle], "")

		for _, line := range candle {
			maxvalue, err := strconv.ParseFloat(tickersMaxAndMinValues[key][maxValuePosition], defaultFloatBitSize)
			if err != nil {
				log.Fatal("error parsing value {", tickersMaxAndMinValues[key][maxValuePosition], "}")
			}

			maxPriceInCandle, err := strconv.ParseFloat(line[maxPricePositionInCandle], defaultFloatBitSize)
			if err != nil {
				log.Fatal("error parsing value {", maxPriceInCandle, "}")
			}

			if maxvalue < maxPriceInCandle {
				tickersMaxAndMinValues[key][maxValuePosition] = line[maxPricePositionInCandle]
				tickersMaxAndMinValues[key][maxValueTimePosition] = line[timePositionInCandle]
			}

			minValue, err := strconv.ParseFloat(tickersMaxAndMinValues[key][minValuePosition], defaultFloatBitSize)
			if err != nil {
				log.Fatal("error parsing value {", tickersMaxAndMinValues[key][maxValuePosition], "}")
			}

			minPriceInCandle, err := strconv.ParseFloat(line[minPricePositionInCandle], defaultFloatBitSize)
			if err != nil {
				log.Fatal("error parsing value {", tickersMaxAndMinValues[key][maxValuePosition], "}")
			}

			if minValue > minPriceInCandle {
				tickersMaxAndMinValues[key][minValuePosition] = line[minPricePositionInCandle]
				tickersMaxAndMinValues[key][minValueTimePosition] = line[timePositionInCandle]
			}
		}
	}

	return tickersMaxAndMinValues
}

func buildOutputValues(processedTrades map[string]map[string][][]string, tickersMaxAndMinValues map[string][]string) map[string][][]string {
	output := make(map[string][][]string)

	for userID, tickers := range processedTrades {
		for ticker, trades := range tickers {
			maxValue, err := strconv.ParseFloat(tickersMaxAndMinValues[ticker][maxValuePosition], defaultFloatBitSize)
			if err != nil {
				log.Fatal("error parsing value {", tickersMaxAndMinValues[ticker][maxValuePosition], "}")
			}

			minValue, err := strconv.ParseFloat(tickersMaxAndMinValues[ticker][minValuePosition], defaultFloatBitSize)
			if err != nil {
				log.Fatal("error parsing value {", tickersMaxAndMinValues[ticker][maxValuePosition], "}")
			}

			maxRevenue := maxValue - minValue

			userProfit := calculateUserProfit(trades)

			output[userID] = append(output[userID], []string{
				userID,
				ticker,
				fmt.Sprintf("%.2f", userProfit),
				fmt.Sprintf("%.2f", maxRevenue),
				fmt.Sprintf("%.2f", maxRevenue-userProfit),
				tickersMaxAndMinValues[ticker][maxValueTimePosition],
				tickersMaxAndMinValues[ticker][minValueTimePosition],
			})
		}
	}

	return output
}

func calculateUserProfit(trades [][]string) float64 {
	var userProfit float64

	for _, trade := range trades {
		purchasePrice, err := strconv.ParseFloat(trade[purchasePricePositionInTrade], defaultFloatBitSize)
		if err != nil {
			log.Fatal("error parsing value {", trade[purchasePricePositionInTrade], "}")
		}

		sellingPrice, err := strconv.ParseFloat(trade[sellingPricePositionInTrade], defaultFloatBitSize)
		if err != nil {
			log.Fatal("error parsing value {", trade[sellingPricePositionInTrade], "}")
		}

		userProfit = userProfit - purchasePrice + sellingPrice
	}

	return userProfit
}

func writeFile(output map[string][][]string, fileName string) error {
	outputFile, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer outputFile.Close()

	writer := csv.NewWriter(outputFile)
	defer writer.Flush()

	for _, lines := range output {
		for _, line := range lines {
			err := writer.Write(line)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
